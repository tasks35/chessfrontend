import {Component} from '@angular/core';
import {AppColumn} from "./interfaces/app-column";
import {DataService} from "./services/data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{

  public columnObjects: AppColumn[] = [];
  public columnStrings: string[] = [];

  constructor(public dataService: DataService) {
    for (var i = 0; i <= 7; i++){
      this.columnObjects.push({Name: `ChessColumn${i}`, Index: i});
      this.columnStrings.push(`ChessColumn${i}`);
    }
  }

}
