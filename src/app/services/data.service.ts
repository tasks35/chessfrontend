import { Injectable } from '@angular/core';
import {FigurePosition} from "../interfaces/figure-position";
import {BehaviorSubject, Observable} from "rxjs";
import {Move} from "../interfaces/move";
import {ChessDataSource} from "../classes/chess-data-source";
import {ajax} from "rxjs/ajax";
import {FigureMove} from "../interfaces/figure-move";
import {map} from "rxjs/operators";
import {Position} from "../interfaces/position";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public bord$ = new BehaviorSubject<FigurePosition[][]>([]);
  public dataSource = new ChessDataSource([]);

  public beginPosition : Position | null = null;
  public endPosition : Position | null = null;
  move$ = new BehaviorSubject<Move>({begin: {X:0,Y:0, Name:''}, end: {X:0,Y:0, Name:''}});

  constructor() {
    this.bord$.subscribe({
      next: x => {
        this.dataSource.setData(x);
      }
    })
  }

  public autoStep(): void{

    this.backendMakeAutoStep().subscribe({
      next: this.updateBoard.bind(this),
      error: err => console.log(err)
    })

  }

  backendMakeAutoStep() : Observable<string[][]> {
    return ajax.getJSON<string[][]>(`http://localhost:5000/Chess/MakeStep`);
  }

  public manualStep(move: FigureMove): void{
    this.backendMakeManualStep(move).subscribe({
      next: this.updateBoard.bind(this)
    });
  }

  backendMakeManualStep(move: FigureMove): Observable<string[][]> {
    return ajax.post(
      'http://localhost:5000/Chess/MakeManualStep',
      JSON.stringify(move),
      {'Content-Type': 'application/json'},
    )
      .pipe(
        map((data) => data.response as string[][])
      );
  }

  public addPosition(position : Position){

    if (this.beginPosition === null){
      this.beginPosition = position;
    }
    else {

      if (this.beginPosition.X === position.X && this.beginPosition.Y === position.Y){
        this.beginPosition = null;
      }
      else {
        this.endPosition = position;
        this.manualStep({OldPosition: this.beginPosition, NewPosition:this.endPosition});
      }

    }
  }

  public updateBoard(value: string[][]): void {
    console.log(`data from backend:`);
    console.log(value);

    let figurePositions : FigurePosition[][] = [];

    for (let i = value.length - 1; i >= 0 ; i--){

      let row: FigurePosition[] = [];

      for (let j = 0; j < value.length; j++){
        row.push({Name: value[i][j], X:j, Y:i});
      }

      figurePositions.push(row);
    }

    this.beginPosition = null;
    this.endPosition = null;

    this.bord$.next(figurePositions);
  }
}
