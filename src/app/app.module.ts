import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatTableModule} from "@angular/material/table";
import { ChessCellComponent } from './components/chess-cell/chess-cell.component';
import { ChessFigureComponent } from './components/chess-figure/chess-figure.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {DataService} from "./services/data.service";

@NgModule({
  declarations: [
    AppComponent,
    ChessCellComponent,
    ChessFigureComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    FontAwesomeModule,
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
