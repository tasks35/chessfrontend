import {Position} from "./position";

export interface FigurePosition extends Position{
  Name: string,
}
