import {Position} from "./position";

export interface FigureMove {
  OldPosition: Position

  NewPosition: Position
}
