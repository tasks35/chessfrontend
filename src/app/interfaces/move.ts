import {FigurePosition} from "./figure-position";

export interface Move {
  begin: FigurePosition,
  end: FigurePosition
}
