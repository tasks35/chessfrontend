import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {
  faChessPawn,
  faChessRook,
  faChessKnight,
  faChessKing,
  faChessQueen,
  faChessBishop
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-chess-figure',
  templateUrl: './chess-figure.component.html',
  styleUrls: ['./chess-figure.component.scss']
})
export class ChessFigureComponent implements OnInit, OnChanges {
  @Input() name: string = "";
  figure = faChessPawn;
  isFigure: boolean = false;
  figureColor = "";

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.isFigure = true;
    switch (this.name.slice(-1)) {
      case 'П':
        this.figure = faChessPawn;
        break;
      case 'Б':
        this.figure = faChessRook;
        break;
      case 'Л':
        this.figure = faChessKnight;
        break;
      case 'С':
        this.figure = faChessBishop;
        break;
      case 'К':
        this.figure = faChessKing;
        break;
      case 'Ф':
        this.figure = faChessQueen;
        break;
      default:
        this.isFigure = false;
    }

    this.figureColor = this.name[0] === "б" ? "white" : "black";
  }
}
