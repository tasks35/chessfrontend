import {Component, Input, OnInit} from '@angular/core';
import {FigurePosition} from "../../interfaces/figure-position";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-chess-cell',
  templateUrl: './chess-cell.component.html',
  styleUrls: ['./chess-cell.component.scss']
})
export class ChessCellComponent implements OnInit {
  @Input() figurePosition : FigurePosition = {X: 0, Y:0, Name:""};

  constructor(public dataService: DataService) { }

  ngOnInit(): void {
  }

  onCellClick(): void{
    this.dataService.addPosition({X: this.figurePosition.X, Y: this.figurePosition.Y});
    console.log(`${this.figurePosition.X} ${this.figurePosition.Y}`);
  }
  isWhite(): boolean {

    if (this.figurePosition.X % 2 == 0){
      if (this.figurePosition.Y % 2 == 0){
        return false;
      }
      else
      {
        return true;
      }
    }
    else {
      if (this.figurePosition.Y % 2 == 0){
        return true;
      }
      else
      {
        return false;
      }
    }
  }
}
