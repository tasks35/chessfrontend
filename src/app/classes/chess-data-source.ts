import {DataSource} from "@angular/cdk/collections";
import {FigurePosition} from "../interfaces/figure-position";
import {Observable, ReplaySubject} from "rxjs";

export class ChessDataSource extends DataSource<FigurePosition[]> {
  private _dataStream = new ReplaySubject<FigurePosition[][]>();

  constructor(initialData: FigurePosition[][]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<FigurePosition[][]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: FigurePosition[][]) {
    this._dataStream.next(data);
  }
}
